.. _opt-introduction:

Throughput Optimization
=======================

We recommend running under slurm using a batch script.

New users may or may not be familiar with slurm or slurm batch scripts.  We
provide a sample slurm batch template which a user can modify with their
specific needs regarding running the actual job.  This should be relatively
a simple process, i.e. if the user already has experience running appliction from the
command line then adjusting the slurm batch script will be rather straightforward.
Link to a template with comments that explain where to modify.
Modifications such as loading the correct modulefiles to access the application,
mpi, and tools as well as the command line they intend to use to launch application.
Additionally, any slurm batch variables etc that may be needed to adjust the job
to the node intended to run the appliction job.

If a user has experience running their application on some other system it would
be useful to have the user provide some information on the hardware resources,
application binary/version, build compiler and other dependent software, mpi
version, OS version, and examples of how they ran on that system. Maybe provide
some idea of how long the run took on that system.  From that information t
hey/we can determine a good starting point for running on parijto compute node
(currently with P8/gpus).

Tools:

* Survey  - this is our performance metric tool that can collect application and
  resource metric use to help optimization and throughput improvement
* Nvidia-smi - this is a tool from nvidia that collects usage metrics on the gpu.

Integration:

Survey is easily added by adding ‘survey’ in front of your mpi command.
Remember to include the module load. Survey will create a directory of thread
detail, a .csv and a .json file that contains the jobs performance metrics.

.. code-block:: console

  module load survey-develop-gcc-7.4.0-4wqt7zo
  survey mpirun -np  4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp

Nvidia-smi is an external process that is started before the job starts and is
killed when it is done. This is best done in the batch script. For this metric
to show job resource use, it is best that only this jobs runs on the node for
collection.

.. code-block:: console

  # start nvidia-smi in background to gather nvidia gpu statistics
  nvidia-smi  --query-gpu=index,utilization.gpu,utilization.memory,power.draw,temperature.gpu,memory.used,memory.free,memory.total --format=csv,nounits -l 5 -f ./meanifulfilename.csv &
  # save nvidia process id
  NVIDIA_SMI_PID=$!
  #
  # run mpi under survey tool
  survey mpirun -np 4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp
  #
  # kill the nvidia-smi process
  kill $NVIDIA_SMI_PID

VASP analysis
-------------

All initial runs should use survey (and nvidia-smi for vasp_gpu) to gather metrics
on how the run is using the hardware resources. We will provide a modulefile to
load in the appropriate tools to do this.

The survey, nvidia-smi, OUTCAR, and devasp output files will be useful
in guiding tuning the job to maximize the job to the available cpu, gpu, memory
resources available on the node.

The list below includes vasp_std and vasp_gpu and there are some important
differences in tuning between the two.  (the P8/gpu nodes seem optimal for
vasp_gpu and may not perform as well with vasp_std compared to other hardware p
latforms - maybe prioritize the vasp_gpu in one section and have vasp_std in
another section?).

#. Determine binary - vasp_std, vasp_gpu, gamma, vasp_ncl

#. User should provide INCAR, POSCAR, POTCAR, optional KPOINTS (and data sets?).

  KPOINTS or KSPACING is the highest priority parameter for optimization of vasp_std
  and vasp_gpu. (need info on single dimension vs 3D specification of KPOINTS)

  Number of atoms determines default KSPACING (or KPOINTS) defaults are 40 (observation)
  KPOINTS, or (in INCAR) KSPACING=.5, KSPACING overrides KPOINTS, possible auto
  default if neither is specified (can we determine default).

  As number of atoms increases, run time and memory use increases geometrically.
  The number of simultaneous runs decreases. (eg. SBATCH -n = allocation/threads
  = runs)

  How to determine for first run:

    ~100 atoms use {40, .5}

    ~500 atoms  use {8 , 2.}

  Second run at a smaller or larger  number of KPOINTS / KSPACING to determine dE
  (from OUTCAR or devasp?) The value is precise.

  KPAR is KPOINTS PARALLELIZATION is suggested to make as large as possible for
  vasp_std and vasp_gpu. We need further quantitative measurements to validate.
  KPAR (default 2 based on P8, otherwise 1) on CPU Memory, if vasp_gpu also look at:

  NSIM (default 16 based on P8, otherwise 4). NSIM is a priority for vasp_gpu.
  Larger NSIM uses more GPU memory. We need more runs to furhter asssess how NSIM
  changes vasp_std runs.

  It has been noted that a measured 10x speedup at NSIM=16 vs NSIM=4. We need
  more runs to see how NSIM changes vasp_std runs.

  Question: If NSIM =1 allows two simultaneous runs, are they faster than two
  single runs back to back?

  We believe we've seen NSIM=4 vs NSIM=16 is better if simultaneous runs can happen.

  NCORE (suggested SQRT{CPUs}), NCORE must =1 for vasp_gpu, can make a big speedup
  for vasp_std when increased. rule of thumb is SQRT(cpu cores). Q: Not usually
  specified in INCAR?

  NPAR less than or equal to NCORE, affects CPU memory.NPAR is NCORE
  PARALLELIZATION we have very little testing on increasing NPAR and how it changes
  memory or time to solution.

3. Environment Variables effects performance

  NUM_OMP_THREADS (default vasp_gpu 1) based on Survey with PGI20. We detected that some library (BLAS) scheduled all possible CPU cores.

  Others? Eg. NUM_BLAS_THREADS?

4. Sweep Tuning based on expected number of runs, and aggregate time to solution

  Initial runs under Survey (for GPUs include nvidia-smi) see example SLURM job
  template.

  Max Simultaneous simulation requires balancing KPAR (CPU memory), NSIM (GPU
  memory) eg. 20% slower, but twice the number of simultaneous runs.

  Never use more than 95% of CPU or GPU memory. VASP varies while running.

  Reduce GPU contention by staggering starts (via sleep command) GPU Util. from
  nvidia-smi (BW). 1,080,000 samples per GPU = ~4M lines into csv for ~500 atoms
  = 3hr run.

  Use example scripts for SLURM batches (recommended).  In some cases a script
  may be generating one or more slurm batch scripts and tuning changes to the
  job should be made there including adding the support required to add survey,
  nvidia-smi to the job.

  Any example template for the actual batch code could/should define a flag such
  that survey,nvidia-smi can be toggled on/off.
  Eg. Create some master variable that controls our job monitoring like
  RUN_SURVEY_TOOLS which when set fills in the variables below as needed, else
  they are empty. This can also control any lines used to start and stop nvidia-smi.

  mpirun -np …..vasp ….
  Becomes:

  .. code-block:: console

    ${SURVEY_TOOL} ${SURVEY_OPTS} mpirun -np …..vasp ….

5. Dedicated vs shared

  Shared may incur restart overhead (eg. wall =8hrs, inner loop time = 3hrs,
  thus one loses 2hrs every 8 of run time or 25%).

6. Analysis of VASP output files, Survey json, csv, nvidia-smi csv output

  OUTCAR - grep LOOP for inner loop times, grep LOOP+ for aggregate time, grep
  time for many collected parametrics.

  POSCAR - Char array, can be used for visualization, ? AI/ML (ask Sven about
  XLOOP).

  devasp_gpu cat to see number of inner loops, 60+ is considered unstable, not
  likely to converge. dE test for precision to validate KPOINTS, KSPACING.

  Zesults  is the summary / reduction of simulation often to six or less,
  sometimes one number.

  survey.json contains the entire runtime shell environment, SLURM info, lscpu
  info, lspci info, metrics collected from survey runtime collector.

  nvidia-smi.csv for GPU runs only, eg. GPU util, power, memory usage number of
  simultaneous runs, temps show average compute loading.

  Extract from all the above as needed to tell the story.

  for these initial runs on the parijto vasp nodes, a tarball of the OUTCAR,
  devasp, survey.json, survey.csv, survey rawdata directory, nvidia-smi.csv,
  slurm job output should be created and sent to trenza for study. Very useful
  in automating the analysis into a potential vasp plugin that would provide a
  summary report.
