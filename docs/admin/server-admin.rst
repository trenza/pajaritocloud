Server Administration
=====================

.. _setuid: runner.html#setuid

Security Considerations
-----------------------
The following should be taken into account
when configuring the GitLab server.

**LDAP**

Official `LDAP documentation <https://docs.gitlab.com/ee/administration/auth/ldap.html#security>`_
provides a clear recommendation, "against using LDAP integration if your LDAP
users are allowed to change their *mail*, *email* or *userPrincipalName*
attribute on the LDAP server or share email addresses." This is
of even a higher important when attempting to manage a `setuid`_
enabled runner.

**SAML**

By default, GitLab uses the NameID returned by the SAML response for the UID.
If your organization's NameID format is transient, the name identifier changes
for each login, thus making it unsuitable to be used as a user identifier.

To address this, the ``uid_attribute`` option should be used, which allows
a SAML 2.0 attribute to be used as the user identifier in place of NameID.

See the docs here: https://docs.gitlab.com/ee/integration/saml.html#uid_attribute

**Account Takeover**

If users are able to change their own email addresses,
auto-linking SAML users should be set to false in the configuration.

.. code-block:: rb

    gitlab_rails['omniauth_auto_link_saml_user'] = false

Otherwise a user can potentially take over another user's account
on the GitLab instance simply by changing their email address.

**Impersonation Through CI Jobs**

If using setUID runners, it should be noted that the runner uses the
GITLAB_USER_LOGIN env variable.
This opens the potential for a GitLab user to impersonate another user
on system resources via CI jobs.

GitLab generates usernames based on email addresses. There is not an option
to configure GitLab to use a different attribute to generate the username.

If a user has not yet created an account, another user can obtain
the username by changing their email address to match that of the
username of the account they wish to compromise.

Another method for impersonating a user through CI jobs
is to directly change the GitLab username.
To prevent this, username changes should be disallowed
by setting the following:

.. code-block:: rb

    gitlab_rails['gitlab_username_changing_enabled'] = false

**Username Changing**

Depending on the mechanism you have used to manage user accounts on the GitLab
instance, by default user can change their associated namespace. If using
`setuid`_ runners change this in your deployment's ``gitlab.rb`` file.

.. code-block:: rb

    gitlab_rails['gitlab_username_changing_enabled'] = false # default: true
