.. _pcc-introduction:

How it works
============

User contracts for resources to be used by individuals or teams. With a focus
of providing a dedicated workload environment, contracts will be Costs
will be associated with the type of node. Current nodes are IBM P8s w/GPUs
(limited quantity) but we expect this to transition over time.

* Our current configuration for lease is on a monthly basis with a discount for
  a yearly lease. An option is to add additional nodes for short term campaigns
  that require large amounts of calculations. There is a weekly option for
  assessment and short term needs.
* Accounts will be assigned to individuals associated with the contract. If a
  small team, this should be limited to 4 and you will need to consider how you
  schedule your team to use.
* Usage - Each node is  configured for interactive or queued jobs via the slurm
  resource manager. Slurm provides finer resource utilization for managing jobs.
  Other scheduling mechanisms and being assessed.
* Software - We will ensure an optimized application build that integrates your
  license (if required) and will provide an environment with additional open
  source tools such as python based and others that are beneficial to you
  analysis needs. We are
  open to suggestions. All software will be available via modules.
* node costs are based on hardware architecture, memory, and type of
  accelerator (gpu). We expect this to evolve over time. Reference our current
  price table below.
* Support - hardware and software environment including the application
  environment. This includes various builds, job management, support tools.
  Customer configurations based on special request.


Environment details
-------------------

Hardware
^^^^^^^^

Rent dedicated compute resources that are targeted at Workloads in
multiples of a node. The current configuration available is an IBM dual
P8 w/4 NVIDIA P100s GPUs, each GPU has 16 GBs of high bandwidth local memory.
The node has system memory of either 256 GB or 1 TB and has 1 TB NFS
storage, additional storage is available based on order agreement. This has
been tested with the currently supported applications for excellent throughput.
Generally one node provides excellent throughput.

For VASP workloads:
^^^^^^^^^^^^^^^^^^^
A 256 GB memory node can run a 256 atom Ti alloy strain simulation in
approximately 1 hour (can run 2 or more simultaneous jobs depending on
simulation size)

A 1 TB memory node can run 4 or more simultaneous jobs in an hour.

Multiple nodes can be pulled together for campaigns. The node(s) are allocated
to the contract and are available 24 hours a day with exception of scheduled
maintenance.

With hardware continuously evolving, we will continuously be testing other
architectures to develop cost effective configurations for running VASP
workloads. Pricing is excellent on these nodes because of market pricing, other
configurations will differ as architectures evolve.

Usage Overview
--------------

Storage
^^^^^^^

Describe home and project space.

Software
^^^^^^^^

PCC will build provide support to build appropriate software needed for your
computing needs. Modules will be setup for ease of use.

* VASP requires your license to be built on leased nodes.

module avail

This will display software packages that have been installed for your use. By
loading modules, they will be incorporated into your run path.

module load <module>

Running jobs
^^^^^^^^^^^^

There are multiple ways to run applications on your node(s).

* The interactive approach places you on the compute node and allows you to run
  on the command line, either jobs or scripts that have been developed.
* The batch submission approach allows you to submit jobs to a work queue,
  either individually or through a submission script where you adjust your
  submissions. This is recommended as job throughout is optimized through job
  submission parameters and the scheduler settings for the node..

Use Startup Instructions
------------------------

Overview
^^^^^^^^

Access - userid and initial PW provided

* Initial connection through portal
* Second connection gets you to front-end node
* salloc will get you to designated node

Storage - 1TB NFS for project

VASP build

* Spack build by PCC

  - With and without VTST
  - tests - PGI performed better in tests
  - If you want your own build - we can help

The node

* 2 cpus - 10 cores/cpu
* 80 threads
* Resources and limitations

Transferring files

We can't push the files off the PCC system. We have to pull from our home systems.

Example from home laptop:

.. code-block:: console

  jgalarowicz@viento:~$ scp -o 'ProxyJump jgalarowicz@pcc-portal.newmexicoconsortium.org' jgalarowicz@po-fe1:/home/jgalarowicz/test.sh test.sh
  ********************************************************************************
    Connections are monitored and recorded. Disconnect if you are not authorized!
  ********************************************************************************
  Password:
  ********************************************************************************
    Connections are monitored and recorded. Disconnect if you are not authorized!
  ********************************************************************************
  Password:
  test.sh                                            100%  908     5.1KB/s   00:00

Working around gateway:

Port Forwarding to your localhost:

.. code-block:: console

  ssh -fN -L 2221:remote_host:22 nmc-portal1
  ssh localhost -p 2221

Proxy Jump:

.. code-block:: console

  ssh -J nmc-portal1 remotehost

Scheduling work - slurm
^^^^^^^^^^^^^^^^^^^^^^^

References for slurm:

  https://slurm.schedmd.com/quickstart.html
  https://curc.readthedocs.io/en/latest/running-jobs/slurm-commands.html

Note – there is only your team partition available:

.. code-block:: console

  pcc-test2@po02:~$ sinfo
  PARTITION  AVAIL  TIMELIMIT  NODES  STATE NODELIST
  pcc-org-01    up   infinite      1    mix po02

example script – test1.batch

.. code-block:: console

  pcc-test2@po02:~$ more test1.batch
  #!/usr/bin/bash
  #SBATCH -n 40
  #SBATCH -p pcc-org-01 -w po02
  #SBATCH -J test-job1
  cd "/home/test2/testdir"
  export OMP_NUM_THREADS=1
  module use /pcc-org-01/project/opt/modulefiles
  module load openmpi-4.0.5-7.4.0-6ix2a2u
  module load vasp-5.4.4.pcc.org.01.novtst-gcc-7.4.0-djlt4as
  mpirun -np  4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp

details below:

.. code-block:: console

  #SBATCH -n 40

This is the number of threads allocated to the job. The total available on the node is 80. This is one of the variables that will allow slurm to schedule more than one job at a time – depending on resources (-n) requested.

.. code-block:: console

  #SBATCH -p pcc-org-01 -w po02
  #SBATCH -J test-job1

-p is your partition, -w is your node, -J is your job name

.. code-block:: console

  cd "/home/pcc-test2/testdir"

cd into your working directory where you have your input files

.. code-block:: console

  export OMP_NUM_THREADS=1

To ensure that only 1 thread is allocated. This is mainly for the PGI-20.*
version of vasp_gpu. Apparently, if this is not set one of the libraries will
take all the threads available to use and causes contention (locking, unlocking)
which causes performance to be degraded by a lot.

.. code-block:: console

  module use /pcc-org-01/project/opt/modulefiles
  module load openmpi-4.0.5-7.4.0-6ix2a2u
  module load vasp-5.4.4.pcc.org.01.novtst-gcc-7.4.0-djlt4as

Tells slurm where your modules are and loads the ones you need for this job. Be aware that you may have multiple builds with different compilers in your module area. Request clarification on compatibility and other dependencies. For instance the qd module is needed when using pgi builds. You can see modules when you salloc -p pcc-nmt-01 into your node and do a module avail

.. code-block:: console

  mpirun -np  4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp

This will run your vasp application. -np 4 will target using the 4 gpus, `which vasp_gpu` will run the appropriate vasp loaded by the module.  > devasp will pipe the output to the devasp file.

Job Throughput
^^^^^^^^^^^^^^

How many jobs can be run

* Depends on size
* Utilize gpus to reduce threads
* 40 is a good # for threads (-n ) std - will allow 2 jobs
* Think of -n as # of cores
* -np is mpi variable - think of as gpus which is equal to 4 (default to this)

VASP parameter options..

* Kpar
* Kpoints
* Kspacing


If you are doing interactive work - not using sbatch

* Coordinate with your team members to ensure you are aware of resource use. If batch jobs are submitted that require all the node resources, you will not be able to do much. Consequently if you are utilizing a large amount of the node resources, jobs will be held up in the job queue.

Metrics and Optimization
^^^^^^^^^^^^^^^^^^^^^^^^

This will initially be done in collabortation with the PCC Optimization team.
Once a process has been developed, much of this could be done by your team.
We provide tools to collect performance and resource information that can help
fine tune run parameters and provide a guide for the number of jos you can run
in parallel. This is very much tied to the type of jobs that you expect to run
and should be done prior to running long suites.

Tools:

* Survey  - this is our performance metric tool that can collect application and resource metric use to help optimization and throughput improvement
* Nvidia-smi - this is a tool from nvidia that collects usage metrics on the gpu.

Integration:

* Survey is easily added by adding ‘survey’ in front of your mpi command.
  Remember to include the module load. Survey will create a directory of thread
  detail, a .csv and a .json file that contains the jobs performance metrics.

.. code-block:: console

  module load survey-develop-gcc-7.4.0-4wqt7zo
  survey mpirun -np  4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp

Nvidia-smi is an external process that is started before the job starts and is
killed when it is done. This is best done in the batch script. For this metric
to show job resource use, it is best that only this jobs runs on the node for
collection.

.. code-block:: console

  # start nvidia-smi in background to gather nvidia gpu statistics
  nvidia-smi  --query-gpu=index,utilization.gpu,utilization.memory,power.draw,temperature.gpu,memory.used,memory.free,memory.total --format=csv,nounits -l 5 -f ./meanifulfilename.csv &
  # save nvidia process id
  NVIDIA_SMI_PID=$!
  #
  # run mpi under survey tool
  survey mpirun -np 4 --mca btl_base_warn_component_unused 0 --map-by socket `which vasp_gpu` > devasp
  #
  # kill the nvidia-smi process
  kill $NVIDIA_SMI_PID

Service Agreement
-----------------

The link below details the service agreement for the VASP workload product.

.. figure:: files/PCC-ServiceTermsConditions_V3.pdf
  :scale: 50%
  :align: left
  :alt: Service Agreement
