Survey Output
=============

The Survey tool generates various outputs that can be controlled via run
options. Multiple runs of the same app will increment the number (#) in the
survey file name. The default file structure is as follow:

json file
---------

<app>-survey-csvdata-#-report.json
This file contains the job and system metadata as well as the summary
performance metrics that include: overall time, DMEM, MEMALLOC, MEMFREE, PAPI,
IO, and MPI

.. image:: files/output_json1.png
  :scale: 35%
  :align: left
.. image:: files/output_json2.png
  :scale: 35%
.. image:: files/output_json3.png
  :scale: 35%
.. image:: files/output_json4.png
  :scale: 35%
.. image:: files/output_json5.png
  :scale: 35%

csv file
--------

<app>-survey-csvdata-#-report.csv
This summary file contains performance metrics that include: overall time,
DMEM, MEMALLOC, MEMFREE, PAPI, IO, and MPI

.. image:: files/output_csv.png
  :scale: 40%
  :align: center
