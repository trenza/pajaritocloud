Report Build Status to GitLab
=============================

This example will introduce you to a workflow that can be leveraged
to update a pipeline's build status on a source GitLab repository. When reading
the official `repository mirroring <https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html>`_
documentation you'll undoubtedly find that there are plenty of resources for
managing a GitHub to GitLab mirror. Though it is easy enough to establish a
GitLab to GitLab mirror, there lacks the same level integration available for
reporting results. Overcoming this can be handled through enhancements
to your project's pipeline.

We will be using the GitLab API to
`post the build status to a specific commit <https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit>`_
in conjunction with a Python script and the
`Requests <https://requests.readthedocs.io/en/master/>`_ library.
Though it may possible to accomplish this using only the command line,
I've found easier to manage and more secure using Python.
If you choose to write your own solution please be aware
that you may be running on a multi-tenant system and to
avoid exposing any token via the command line.

.. literalinclude:: ../../source/build-status-gitlab/build-status.py
    :language: python

In the above script we identify all variables from the command's environment.
You may recognize that we have used several
`predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_
that are provided to each CI job. There are also
`variables <https://docs.gitlab.com/ee/ci/variables/README.html>`_
we've defined at the project level that are directly related to
our upstream repository:

* **BUILDSTATUS_PROJECT**: The upstream project ID.
* **BUILDSTATUS_APIURL**: The repositories API url (e.g. https://gitlab.com/api/v4)
* **BUILDSTATUS_TOKEN**: An API scoped personal access token.

We strongly advise that variables, such as the **BUILDSTATUS_TOKEN** in
our example, are
`masked <https://docs.gitlab.com/ee/ci/variables/README.html#masked-variables>`_
to avoid exposing them publicly. These variable names are arbitrary and you can
rename them to whatever works best in your project's structure.

    .. image:: files/project-variables-status.png
        :scale: 40%

Finally once you've updated your project's CI/CD settings and added
the necessary script you should incorporate it into your pipeline
(the ``.gitlab-ci.yml`` file).

.. tabs::

    .. tab:: Generic

        .. literalinclude:: ../../source/build-status-gitlab/generic.gitlab-ci.yml
            :language: yaml

After a pipeline is triggered from the mirror the upstream GitLab's pipeline
should appear as such:

    .. image:: files/pipeline-mirror-status.png
        :scale: 50%

The *test* job in our example is some arbitrary job executed by the upstream
repo. Our script is responsible for adding the *Example Job* which will
redirect users to the ``CI_PIPELINE_URL``. Please note that you are
responsible for ensuring that both the project and pipelines are publicly
visible for your mirror project.
