Running Survey
==============

Survey operates by utilizing LD_PRELOAD to wrap function calls in your
application. To do this, survey is called before your application and sets the
stage for collection. The normal usage (developer use case) is:

.. code-block:: console

    $ survey "mpirun -np ./mpiapp"

This will collect all the survey metrics (default) for mpiapp. Options to
disable specific collectors or add specific program counters are add prior
to running the application and its options. Other uses cases (testing,
monitoring, CI) will include integrated options for metric options in case a
more limited set of metrics is needed. We are also working on a configuration
file for options to simplify the run process.

Run Options
-----------

.. code-block:: console

    $ survey --help

usage: survey
    survey arguments
            survey  [-h] [--counters COUNTERS] [--disable-papi-multiplex]
            [--disable-papi] [--disable-io] [--disable-mem] [--disable-mpi]
            [--disable-ompt] [--top-down] [--top-down-fe] [--top-down-bs]
            [--top-down-be] [--top-down-ret] [--top-down-mem-bound]
            [--top-down-core-bound] [--view_dir VIEW_DIR] [--io] [--memory]
            [--papi] [--summary] [--rusage] [--mpi] [--ompt] [--ranks RANKS]
            [--threads THREADS] [--info INFO] [--advice] [--csv-only]
            [--json-only] [--debug_collector] [--debug_monitor]
            [target]

Survey tool for collecting and displaying measurements

positional arguments:
  target    Where target is the program you want to collect data
            from (e.g. "mpirun -np 2 ./mpiapp")

optional arguments:
  -h, --help        	show this help message and exit

collection options:
  Options to control the collector runtime.

  --counters COUNTERS   Counters to collect. Accepts both papi and papi_native
                      counter names separated by commas
  --disable-papi-multiplex  Disables papi multiplex counters (see documentation
                      for details). default is enabled
  --disable-papi      Disables papi counter collection. default is enabled
  --disable-io      	Disables tracking posix IO calls. default is enabled
  --disable-mem     	Disables tracking memory calls. default is enabled
  --disable-mpi     	Disables tracking mpi calls. default is enable When
                      program uses mpi
  --disable-ompt    	Disables openmp ompt callbacks. default is enabled when
                      program uses openmp

topdown options:
  Options for topdown collection.

  --top-down        	top-down analysis
  --top-down-fe     	front-end category of top-down analysis
  --top-down-bs     	bad speculation category of top-down analysis
  --top-down-be     	back-end category of top-down analysis
  --top-down-ret    	retiring category of top-down analysis
  --top-down-mem-bound  back-end sub-category of top-down analysis
  --top-down-core-bound
                      back-end sub-category of top-down analysis

viewing data options:
  Options for viewing data.

  --view_dir VIEW_DIR   directory of existing data to analyze
  --io              	io details
  --memory          	memory details
  --papi            	papi measurements and calculations
  --summary         	summary details including timing, etc
  --rusage          	rusage details
  --mpi             	mpi details
  --ompt            	omp details
  --ranks RANKS     	ranks to analyze (e.g. "x,y,z" or "x:z" or "x")
  --threads THREADS 	ranks to analyze (e.g. "x,y,z" or "x:z" or "x")
  --info INFO       	Get info about a measurement or calculation
  --advice          	Provide advice from test results

output file options:
  Options for output files. Default is both csv and json.

  --csv-only        	only output results in csv format
  --json-only       	only output results in json format

debug options:
  Options to enable debug from the collector runtimes.

  --debug_collector 	Enable debug from the collector runtime
  --debug_monitor   	Enable debug from the libmonitor runtime

Top-Down metrics
----------------

Describe top down data collection and analysis


Below is old stuff from ECP - get rid of later
----------------------------------------------


Example projects have been created and verified with the HPC enhanced executor
to provide not only recommended practices but also an interactive avenue to
better understand GitLab CI. Even if you already have experience with the
tools we've taken the time to also highlight HPC
focused examples that can assist you and your team.

.. list-table::
    :header-rows: 1
    :widths: 20, 4, 4, 4, 4, 40

    * - Title
      - ALCF
      - OLCF
      - NERSC
      - GitLab
      - Description
    * - `GitLab CI Basics <examples/gitlab-ci-basics.html>`_
      - √
      - √
      - x
      - √
      - A simple multi-stage "Hello World" build/test pipeline.

Guides (this is ECP, but see if we can use this format)
-------------------------------------------------------

Guides have been written to highlight both best practices as well as
potential workflows you may want to leverage when using both facility
and ECP CI resources.

.. toctree::
   :maxdepth: 1

   guides/manual-directory-cleanup.rst
   guides/build-status-gitlab.rst
   guides/multi-gitlab-project.rst
   guides/hpc-artifacts-caching.rst
