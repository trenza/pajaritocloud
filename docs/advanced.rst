Advanced Use
============

The survey tool will evolve its capability for advanced user analysis through
additional metric collection and analysis tools based on interaction with users.
These capabilities may be deployed as options and may have constraints. An
example is ability to collect and save per thread data for analysis that may
not be viable option for high scale runs.

This section will continuously changing during our beta development phase.

Output
------

Survey does collect per thread data that is used to generate the summary data
provided in the csv and json files. This is rich data that can feed further
analysis. We are assessing how to apprach this for large scale runs.

.. image:: files/output_dir.png
  :scale: 40%
  :align: right

Thread level directory
----------------------

<app>-survey-cvsdata-#
This is a directory that contains a csv file for each process allocated to
the job plus a json file with the job/system metadata

.. image:: files/output_thread_dir.png
  :scale: 35%
  :align: right


<node name>-<proc #> -
Each file contains summary or min/max/average values for each process within a
node.

The following is collected at a detail level:

* host,pid,rank,tid,executable,total_time_seconds
* maxrss_kB,utime_seconds,stime_seconds
* dmem_size_kB,dmem_resident_kB,dmem_high_water_mark_kB,dmem_shared_kB,
  dmem_hep_kB
* Io_total_time_seconds,read_time_seconds,write_time_seconds, read_bytes,
  write_bytes
* allocation_time_seconds,allocation_calls,allocation_bytes
* free_time_seconds,free_calls
* total_mpi_time_seconds
* PAPI counters - PAPI_TOT_CYC, PAPI_TOT_INS, PAPI_DP_OPS, PAPI_FP_INS,
  PAPI_FP_OPS, PAPI_TLB_IM
