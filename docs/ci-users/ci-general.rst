General CI Details
==================

CI Job Token
------------

Each CI job has associated with it a unique
`Job token <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html#job-token>`_
that can be used by the user to gain read access to project and support basic
API interactions with the associated GitLab instance. This token is scoped
to the user and available only for the duration of the job itself. The
server takes step to mask the token so it can not be seen from the
web ui.

  .. image:: files/gitlab_web_token.png
      :scale: 80%

Regardless of the steps taken by the server to secure the token there are
ways it can be miss handled in multi-tenant environments.

The following subsections each detail suggested workflows that should be used
in your CI to best handle the provided token. Please note that usage of these
is completely optional and in some cases, depending on the target test
environment, may be unnecessary.

Git
~~~

* Added in `HPC Runner v0.5.0 <../releasenotes/runner_0.5.0.html>`_

Traditional GitLab documentation has you passing the ``CI_JOB_TOKEN`` via
the command line in Git commands. This is potentially unwise if performed on
a multi-tenant environment. As such the runner will properly set the
`GIT_ASKPASS <https://git-scm.com/docs/gitcredentials>`_ environmental
variable and generate the associated file.

.. code-block:: console

    $ git clone https://gitlab-ci-token@gitlab.example.com/group/project.git
    Cloning into 'project' ...

In the above example the `project` repository is private but stored in the
same GitLab instance as the job itself. By specifying the **gitlab-ci-token**
user we are able to easily clone the repository without being forced to supply
the token via a command line argument.

CI Job Scheduling
-----------------

CI job pipelines can be triggered under a number of conditions, including but
not limited to; commits, merge requests, and schedules. Pipeline schedules
can be configured to ensure execution of specific pipelines occur on
project defined intervals. Such scheduling could useful for teams that wish
to establish nightly regression testing for instance. For more details on
how to setup a `pipeline schedule <https://docs.gitlab.com/ee/ci/pipelines/schedules.html>`_
please see the official documentation.

CI Rules
~~~~~~~~

GitLab offers a number of potential mechanisms by which we can
trigger a pipeline (e.g. via the web, on a scheduler, by a merge request,
etc.). We can even choose to use this in how we may limit which jobs should
be run. For example:

.. code-block:: yaml

  example-job:
    rules:
      - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedules"'
        when: always
    script:
      - test

`Rules <https://docs.gitlab.com/ee/ci/yaml/#rules>`_ was introduced in
GitLab server version *12.3*. It allow us to define when jobs might be
expected to run. In this case it is only
`if <https://docs.gitlab.com/ee/ci/yaml/#rulesif>`_ the source of the
pipeline's trigger was either manually via the ``web`` or ``scheduled``.
In the case of GitLab ``web`` refers to triggering the pipeline using
the *Run Pipelines* button, found in the ``CI/CD`` --> ``Pipelines``
page. Take note that with these rules, even though they are evaluated server
side, you can leverage `CI Variables <https://docs.gitlab.com/ee/ci/variables/>`_.
We encourage you to look into the upstream documentation for more information
but suffice it to say there is a lot of potential control provided
over jobs with these mechanisms.
