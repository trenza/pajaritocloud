VASP Workloads
==============

Pajarito Cloud VASP workload compute products are targeting dedicated
availability of compute node resources for VASP jobs to match your research and
development workloads for material studies. The resources have been selected to
optimize VASP throughput with the compute hardware configured based on VASP
needs and limitations. We have described the process to match compute hardware
to applications as developing an instrument for computing. In this case
developing the VASP instrument took time to benchmark and prototype in order to
get effective configurations. You will find that costs and time to solution for
dedicated resources are exceptional when compared to traditional HPC job
scheduling and resource use.
