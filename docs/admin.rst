Administration
==============

.. _introduction: introduction.html#enhancements

Documentation and guides to support administrating GitLab
runners and servers targeting HPC environments. The information
provided here is meant to supplement the
`official documentation <https://docs.gitlab.com/>`_ in areas
where it does not address HPC focused questions in addition to
detailing all ECP enchantments.

Runners
-------

The `GitLab Runner <https://gitlab.com/gitlab-org/gitlab-runner>`_ is
an application that supports the execution of CI/CD jobs as well as ensuring
results are reported back to an associated GitLab server. As outlined
in the introduction_ specific enhancements (setuid, batch execution, and
federations) have been implemented with an aim to
best support HPC targeted workflows. These sections detail how
an administrator can deploy and manage this runner.

.. toctree::
   :maxdepth: 2

   admin/runner-deploy.rst
   admin/runner-admin.rst
   admin/setuid.rst
   admin/runner-batch.rst

Compatibility Chart
~~~~~~~~~~~~~~~~~~~

See the official
`Comparability chart <https://docs.gitlab.com/runner/executors/#compatibility-chart>`_
for the complete list of supported features by different executors. We attempt
to preserve all original functionality and simply add to the following:

+------------+-------+-------+------+-------+------------+
| Executor   | Batch | Shell | SSH  | Docker| VirtualBox |
+============+=======+=======+======+=======+============+
| Setuid     | √     | √     | x    | x     | x          |
+------------+-------+-------+------+-------+------------+
| Federation | √     | √     | x    | x     | x          |
+------------+-------+-------+------+-------+------------+
| HPC        | √     | x     | x    | x     | x          |
| Scheduler  |       |       |      |       |            |
+------------+-------+-------+------+-------+------------+
| Token      | √     | √     | [1]_ | [1]_  | [1]_       |
| handling   |       |       |      |       |            |
+------------+-------+-------+------+-------+------------+

.. [1] Further testing is required to ensure target functionality

All enhancements target HPC and have been focused on the Shell and Batch
executors **only**. We currently have no plans to extend support to the other
executor types. It is envisioned that the runners installed on HPC systems are
targeting the locally available resources, traditional Docker or Kubernetes
executors would reside on separate physically infrastructure. As such we
recommend use of the enhanced runner for batch / shell only.

Server
------

`GitLab <https://about.gitlab.com/what-is-gitlab/>`_ offers a fairly
comprehensive development platform all within a single application. A major
benefit to using this tool is predominately
`open source <https://gitlab.com/gitlab-org/gitlab>`_ and allows for
management of self-hosted instances. However there are a
`number of features <https://about.gitlab.com/pricing/self-managed/feature-comparison/>`_
that require licenses.

The documentation organized here is not written to supplant anything
`official <https://docs.gitlab.com/>`_ offered by GitLab. Similar to
Runners_ we only seek to highlight specifics that are directly relating to
HPC centers and/or custom forks of the server. We fully advise using GitLab's
documentation wherever possible as it often provide very up-to-date and
accurate information.

.. toctree::
   :maxdepth: 2

   admin/server-admin.rst

Federation
~~~~~~~~~~

At the core of the ECP enhancement efforts is the concept of federation
With this we are targeting expanding GitLab's current authentication model
to allow the management of multiple providers to expand beyond
the login process. Fundamentally a user's valid authentication
providers will become part of the CI job process.

Though we are currently testing these enhancements there is an
active effort to improve and ensure that they are merged into
upstream GitLab. You can find all current discussions associated
with `issue 33665 <https://gitlab.com/gitlab-org/gitlab/issues/33665>`_.

Supporting meaningful documentation that provides value to any
system administrator responsible for deployment is an important to us.
As such we are actively working to organize such documentation
in conjunction with ongoing federation improvement efforts and will update
once more information is available.

Auditing
--------

Auditing a user's interactions with a GitLab deployment is an important
administrative component. Although the runner may be completely open-source
there are aspects of the server, including
`Audit Event <https://docs.gitlab.com/ee/administration/audit_events.html>`_,
that require a paid subscription. To understanding logging and what can
be accomplished with your given license please see the official
`log system <https://docs.gitlab.com/ee/administration/logs.html>`_
administrator documentation.

Release Notes
-------------

Detailed release notes for all runner and server development efforts. Due to
the nature of managing multiple forks as well as distinct HPC focused libraries
centralizing notes hopefully allows for easier review as well as
clearer documentation of the changes for both administrators and users alike.

Runner
~~~~~~

.. toctree::
   :maxdepth: 1

   releasenotes/runner_0.6.1.rst
   releasenotes/runner_0.6.0.rst
   releasenotes/runner_0.5.4.rst
   releasenotes/runner_0.5.3.rst
   releasenotes/runner_0.5.2.rst
   releasenotes/runner_0.5.1.rst
   releasenotes/runner_0.5.0.rst
   releasenotes/runner_0.4.3.rst
