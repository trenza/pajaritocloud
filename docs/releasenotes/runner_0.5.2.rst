HPC GitLab-Runner Release 0.5.2
===============================

* *Release Date*: 12/12/2019
* *Commit*: 4b30b4d1
* *Tag*: `v12.4.0-hpc.0.5.2 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v12.4.0-hpc.0.5.2>`_

General Changes
---------------

* To assist in troubleshooting jobs on potentially remotes systems the
  runner's local timestamp will be printed in the output.

 - .. code-block:: console

    Running with gitlab-runner 12.4.0~hpc.0.5.2
      on Test Runner
      job starting 2019-12-12 15:52:35

* Cobalt's logic has been improved to better identify the job id in addition
  to the final exit status.

* Jobs submitted using Cobalt will include the `Submitted batch job #` in
  the CI output.

Admin Changes
-------------

* Jobs submitted to Cobalt will log their final ''Exit_status=?`` upon job
  completion. The location of these logs are now configurable by the
  runner administrator via the `config.toml`.
  - .. code-block:: toml

    [[runners]]
      name = "example-cobalt-runner"
      scheduler_accounting_logs = "/var/log/cobalt"

  - The default value is ``/var/log/pbs`` and it is required that logs in
    this directory be named with the pattern `yyyymmdd`

  - Processing the logs is handled by the GitLab runner user, **not** the
    CI user as they traditionally do not have access to these logs.

  - The runner will use the defined NFS timeout interval (1 minute) when
    checking for the job. After this a job will fail due to timeout.

* Cobalt job id is obtained through the stdout of a successful ``bsub``
  command and will now attempt to parse the job id regardless of queue
  or other potential ``bsub`` arguments.

  - Any failure to find a job id will will have the following message
    associated, "Failed to identify jobid from qsub stdout".
