.. _pcc-introduction:

Environment
===========

Resources
---------

Compute Resources
^^^^^^^^^^^^^^^^^

Pajarito Cloud will be adding additional compute architectures as appropriate
for users and compute products to support other applications that are being
introduced into the Center. At this time we are focused on providing products
that support VASP workloads and have identified a cost effective
platform for use. These will evolve over time with the commitment of cost
effective environment for optimum throughput.

-  12 IBM Power 8, minskey board with NIVIDIA P100's

Storage
^^^^^^^

There will be 1TB NFS storage per project to support team environments.
Storage space will only be mounted on nodes that are allocated to users.


Job Scheduling
^^^^^^^^^^^^^^

We are utilizing slurm to support optimum scheduling of jobs within your node
to take into consideration available threads, memory and gpu's. This can be
expanded to multiple nodes if you decide to add additional compute units to your
contract.

Support
^^^^^^^

We will provide software environment and hardware issue support based on the
needs of the product environment.

Security
^^^^^^^^
