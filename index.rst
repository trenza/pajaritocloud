Welcome to Pajarito Cloud Computing
===================================

.. image:: docs/files/RR.cloud.test.png
  :scale: 12%
  :align: right


Pajarito Cloud is a specialty HPC center for specific workloads. The  VASP
compute products are the initial focus but we expect to optimize other compute
instruments for other applications that require a greater tie between hardware
configuration and the application capabilities. To support this goal, we are
integrating a baselining and benchmarking process to further optimization and
we expect to add a Continuous Integration (CI) testing
service that will include multiple hardware architectures that will provide a
service to project teams that need to run across multiple architectures as well
as provide a forum to optimize configurations for application instruments.


.. warning::

  Documentation is currently under development, please be aware that changes
  to structure and contents will occur.

.. toctree::
   :caption: Contents
   :maxdepth: 2

   docs/environment.rst
   docs/howitworks.rst
   docs/throughput.rst
   docs/vaspworkload.rst
..   docs/running.rst
..   docs/data.rst
..   docs/advanced.rst
..   docs/admin.rst
